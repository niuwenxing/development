package com.example.routelibrary;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class RRouter {

    public static void startActivity(String activityName){
        startActivity(null,activityName,null,-1);
    }

    public static void startActivity(String pakname,String activityName){
        startActivity(null,pakname,activityName,null,-1);
    }

    public static void startActivity(String activityName,Bundle param){
        startActivity(null,activityName,param,-1);
    }


    public static void startActivity(@Nullable Object starter,String activityName,Bundle param){
        startActivity(starter,activityName,param,-1);
    }


    public static void startActivity(@Nullable Object starter, String activityName, Bundle param, int requestCode){
        Router.Navigation navigation = Router.getInstance().setDestination(activityName).putExtras(param);
        if(starter instanceof Fragment){
            navigation.navigate((Fragment) starter, requestCode);
        }else if(starter instanceof Activity){
            navigation.navigate((Context) starter, requestCode);
        } else {
            navigation.navigate((Context)null, requestCode);
        }

    }
    //跳转外部App
    public static void startActivity(@Nullable Object starter,String pakname, String activityName, Bundle param, int requestCode){
        Router.Navigation navigation = Router.getInstance().setDestination(pakname,activityName).putExtras(param);
        if(starter instanceof Fragment){
            navigation.navigate((Fragment) starter, requestCode);
        }else if(starter instanceof Activity){
            navigation.navigate((Context) starter, requestCode);
        } else {
            navigation.navigate((Context)null, requestCode);
        }

    }

}
