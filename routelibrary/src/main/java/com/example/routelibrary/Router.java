package com.example.routelibrary;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Router {
    private static final String TAG=Router.class.getName();

    private static boolean initialized=false;

    private static Context context;

    private static final Map<String, String> routerMap = new HashMap<>();

    private static final Router router = new Router();

    public static Router getInstance(){
        return router;
    }

    private Router(){}

    public synchronized static void init(Context context){
        if (initialized) {
            return;
        }
        Router.context=context;
        if (context==null) {
            return;
        }
        initRouter(context);
        initialized=true;//规避多次初始化
    }

    //设置路径 path Activity
    public Navigation setDestination(String path){
        Navigation navigation = new Navigation();
        navigation.setDestination(path);
        return navigation;
    }
    //设置路径 path Activity
    public Navigation setDestination(String pakname,String path){
        Navigation navigation = new Navigation();
        navigation.setDestination(pakname,path);
        return navigation;
    }


    static class Navigation {
        String destination;
        Intent intent = new Intent();
        Bundle options;
        public Navigation setOptions(Bundle options) {
            this.options = options;
            return this;
        }

        public Navigation putExtras(Bundle bundle) {
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            return this;
        }
        //设置路径
        public void setDestination(String path) {
            destination = routerMap.get(path);
            if (destination!=null) {
                intent.setComponent(new ComponentName(context,destination));
            }
        }
        //设置 包名 activity
        public void setDestination(String PackageName,String path) {
                intent.setComponent(new ComponentName(PackageName,PackageName+"."+path));
        }

        //Fragment
        public void navigate(Fragment context, int requestCode) {
            if (!initialized) {
                throw new NullPointerException();
            }
            if(intent==null){
                return;
            }
            try {
                if (context != null) {
                    if (requestCode >= 0) {
                        context.startActivityForResult(intent, requestCode);
                    } else {
                        context.startActivity(intent, options);
                    }
                } else {
                    startActivity(null, requestCode);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        //activity
        public void navigate(Context context, int requestCode) {
            if (!initialized) {
                return;
            }
            if(intent==null){
                return;
            }
            Context startContext=context;
            if (context==null) {
                startContext=Router.context;
            }
            startActivity(startContext,requestCode);
        }



        private void startActivity(Context context, int requestCode){//onActivityResult requestCode>0 返回值
            if (context==null) {
                return;
            }
            try {
                if(context instanceof Activity && requestCode>=0){
                    ActivityCompat.startActivityForResult((Activity) context,intent,requestCode,options);
                }else{
                    if (!(context instanceof Activity)) {//标记 NEW
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    ActivityCompat.startActivity(context, intent, options);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }




    private static ActivityInfo[] activityInfos=null;
    private static void initRouter(Context context) {

        PackageManager packageManager = context.getPackageManager();
        List<String> activityNames = new ArrayList<>();
        try {
            //获取包名下 所有 注册 activity / packageInfo.activities
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
            //receivers|services|providers
            activityInfos = packageInfo.activities;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (activityInfos!=null) {
            for (ActivityInfo activityInfo : activityInfos) {
                activityNames.add(activityInfo.name);
            }
        }
        for (String activityName : activityNames) {
            String[] splitStr = activityName.split("\\.");
            routerMap.put(splitStr[splitStr.length - 1], activityName);
        }
    }

}
