package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import com.example.myapplication.databinding.ActivityLaunchpageBinding;
import com.example.routelibrary.RRouter;
import com.example.routelibrary.Router;


public class LaunchpageActivity extends AppCompatActivity {
    private  static final String TAG =  LaunchpageActivity.class.getName();
    ActivityLaunchpageBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=DataBindingUtil.setContentView(this,R.layout.activity_launchpage);
        setContentView(binding.getRoot());
        Router.init(getApplication());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RRouter.startActivity("HomepageActivity");
                finish();
            }
        },5000);
        PackageManager packageManager = getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(this.getPackageName(), 0);
            binding.visercode.setText(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
