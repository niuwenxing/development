package com.example.businesslogic.activity.util;

import android.content.Context;
import android.util.Log;

import com.example.businesslogic.R;

import io.agora.rtc.RtcEngine;

public class EngineStr {

    private final String TAG=EngineStr.class.getName();
    private EngineStr engineStr=null;

    public EngineStr(){}

    public Context context=null;

    public MyEngineEventHandler mEventHandler;

    public EngineStr getEngineStr(Context context){
        engineStr.context=context;
        if (engineStr==null) {
            return engineStr =new EngineStr();
        }
        return engineStr;
    }

    public void init(){
        if (context==null) {
            Log.e(TAG, "init: context=null" );
            return;
        }
        mEventHandler = new MyEngineEventHandler();
        try {
            RtcEngine.create(context,context.getString(R.string.agora_app_id),mEventHandler);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }





}
